<?php

class dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('status') != "login"){
			redirect(base_url("auth"));
        }
        
        $this->load->database();
        $this->load->model('project_model');
    }

    public function index()
    {
        $id = $this->session->userdata('id');
        // Get All Data Counter
        $date['date']= date('d-m-Y');
        $data['project']= $this->project_model->getTop5ByUserId($id); 
        $data['countProject']= $this->project_model->getCountProject($id); 
        $data['countTask']= $this->project_model->getCountTask($id); 

        $data['allProject']= $this->project_model->getProjectByUserId($id); 
        
        // Get Status Project
        $suksesProject = 0;
        for($i=0; $i< sizeof($data["allProject"]); $i++) {
            if ($data['allProject'][$i]['statusProject'] == 'Finished') {
                $suksesProject++;
            }
        }
        if (sizeof( $data["allProject"])==0) {
            $data["suksesProjectPersen"] =  0;
        } else {
            $data['suksesProjectPersen']=round($suksesProject/sizeof($data["allProject"])*100);
        }

        // Get List Project
        for($i=0; $i< sizeof($data["project"]); $i++) {
            $task = $this->project_model->getTaskByProject($data["project"][$i]['idProject']);
            $finishedTask = 0;
            for($j=0; $j< sizeof($task); $j++) {
                if ($task[$j]['statusTask']==2) {
                    $finishedTask++;
                }
            }
            if (sizeof($task)==0) {
                $data["project"][$i]['progressTask'] =  0;
            } else {
                $data["project"][$i]['progressTask'] =  $finishedTask/sizeof($task)*100;
            }
        }

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view('dashboard/dashboard',$data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal',$date);
    }

}
