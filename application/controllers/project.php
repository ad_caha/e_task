<?php

class project extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('status') != "login"){
			redirect(base_url("auth"));
        }
        
        $this->load->database();
        $this->load->model('project_model');
    }
    public function addProjectshow(){
        $date['tanggal'] = date('d-m-y');
        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view('dashboard/addProject',$date);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal');
    }
    public function addProject()
    {
        date_default_timezone_set("Asia/Bangkok");
        $namaProject = $this->input->post('namaProject');
        $date = date('Y-m-d H:i:s');
        $updateby = $this->session->userdata("id");
        $statusProject = "Tahap Awal";
        $approval = "false";
        $idPegawai = $this->session->userdata("id");

		$data = array(
            'namaProject' => $namaProject,
            'tanggalProject' => $date,
            'updateBy' => $updateby,
            'statusProject' => $statusProject,
            'approval' => $approval,
            'idPegawai' => $idPegawai
			);
		$this->project_model->addDataProject($data,'project');
		redirect('dashboard');
    }

}