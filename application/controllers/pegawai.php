<?php


class pegawai  extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('status') != "login"){
			redirect(base_url("auth"));
        }
        
        $this->load->database();
        $this->load->model('pegawai_model');
    }
    public function index()
    {

        $jabatan = $this->session->userdata('idjabatan');
        
        if($jabatan =='Head')
        {
            $data['project']=$this->pegawai_model->getAllPegawai();
        } else if ($jabatan =='CO')
        {
            $bawahan = 5;
            $bidang = $this->session->userdata('bidang');
            $table = "user";
            $data['project']=$this->pegawai_model->getAllPegawaiByBidang( $bawahan, $bidang, $table ); 
        } else if ($jabatan == 'St')
        {
            redirect(base_url("dashboard"));
        }

        $date['date']= date('d-m-Y');
        
        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view('dashboard/pegawai',$data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal');
    }

}