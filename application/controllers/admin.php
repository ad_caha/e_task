<?php

class admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('status') != "login"){
			redirect(base_url("auth"));
        }
        if(!$this->session->userdata('admin')){ 
            redirect(base_url("dashboard"));
        }
        
        $this->load->database();
        $this->load->model('m_admin');
    }

    public function index()
    {
        $data['countUser']= $this->m_admin->getCountUser(); 
        $data['countJabatan']= $this->m_admin->getCountJabatan(); 
        $data['countBagian']= $this->m_admin->getCountBagian(); 
        $date['tanggal'] = date('d-m-y');
        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view('dashboard/admin/admin',$data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal');
    }

    // User Function
    public function listuser()
    {
        $data['user']= $this->m_admin->getAllUsers(); 

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view('dashboard/admin/user/list',$data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal');
        $this->load->view('dashboard/admin/adminModal');
    }

    public function adduser()
    {
        
        $date['date']= date('d-m-Y');
        $data["jabatan"] = $this->m_admin->getAllJabatan(); 
        $data["bagian"] = $this->m_admin->getAllBagian(); 

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view("dashboard/admin/user/add", $data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal',$date);
    }

    public function edituser($id = null)
    {
        if (!isset($id)) redirect('admin/listuser');
        
        $date['date']= date('d-m-Y');
        $var["user"] = $this->m_admin->getUser($id); 
        $data["user"] = $var["user"][0];
        $data["jabatan"] = $this->m_admin->getAllJabatan(); 
        $data["bagian"] = $this->m_admin->getAllBagian(); 

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view("dashboard/admin/user/edit", $data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal',$date);
    }
    
    public function deleteuser($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->m_admin->deleteUser($id)) {
            redirect(site_url('admin/listuser'));
       
        }
    }

    public function updateuser()
    {
        $post = $this->input->post();
        $data = array(
            'id' => $post["id"],
            'username' => $post["username"],
            'firstname' => $post["firstname"],
            'NIP' => $post["NIP"],
            'email' => $post["email"],
            'jabatan' => $post["jabatan"],
            'idBagian' => $post["bagian"]
        );

        $this->m_admin->updateuser($data);
        
		redirect('admin/listuser');
    }

    public function createuser()
    {
        $post = $this->input->post();
        $data = array(
            'username' => $post["username"],
            'password' => md5($post['password']),
            'firstname' => $post["firstname"],
            'NIP' => $post["NIP"],
            'email' => $post["email"],
            'jabatan' => $post["jabatan"],
            'idBagian' => $post["bagian"]
        );

        $this->m_admin->createuser($data);
        
		redirect('admin/listuser');
    }

    // Jabatan Function
    public function listjabatan()
    {
        $data['jabatan']= $this->m_admin->getAllJabatan(); 

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view('dashboard/admin/jabatan/list',$data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal');
        $this->load->view('dashboard/admin/adminModal');
    }

    public function addjabatan()
    {
        
        $date['date']= date('d-m-Y');
        $data["jabatan"] = $this->m_admin->getAllJabatan(); 
        $data["bagian"] = $this->m_admin->getAllBagian(); 

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view("dashboard/admin/jabatan/add", $data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal',$date);
    }

    public function editjabatan($id = null)
    {
        if (!isset($id)) redirect('admin/listjabatan');
        
        $date['date']= date('d-m-Y');
        $var["jabatan"] = $this->m_admin->getJabatan($id); 
        $data["jabatan"] = $var["jabatan"][0];

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view("dashboard/admin/jabatan/edit", $data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal',$date);
    }

    public function deletejabatan($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->m_admin->deleteJabatan($id)) {
            redirect(site_url('admin/listjabatan'));
    
        }
    }

    public function updatejabatan()
    {
        $post = $this->input->post();
        $data = array(
            'id' => $post["id"],
            'Id_Jabatan' => $post["Id_Jabatan"],
            'Nama_Jabatan' => $post["Nama_Jabatan"]
        );

        $this->m_admin->updatejabatan($data);
        
        redirect('admin/listjabatan');
    }

    public function createjabatan()
    {
        $post = $this->input->post();
        $data = array(
            'Id_Jabatan' => $post["Id_Jabatan"],
            'Nama_Jabatan' => $post["Nama_Jabatan"]
        );

        $this->m_admin->createjabatan($data);
        
        redirect('admin/listjabatan');
    }

    // Bagian Function
    public function listbagian()
    {
        $data['bagian']= $this->m_admin->getAllBagian(); 

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view('dashboard/admin/bagian/list',$data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal');
        $this->load->view('dashboard/admin/adminModal');
    }

    public function addbagian()
    {
        
        $date['date']= date('d-m-Y');
        $data["jabatan"] = $this->m_admin->getAllJabatan(); 
        $data["bagian"] = $this->m_admin->getAllBagian(); 

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view("dashboard/admin/bagian/add", $data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal',$date);
    }

    public function editbagian($id = null)
    {
        if (!isset($id)) redirect('admin/listbagian');
        
        $date['date']= date('d-m-Y');
        $var["bagian"] = $this->m_admin->getBagian($id); 
        $data["bagian"] = $var["bagian"][0];

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view("dashboard/admin/bagian/edit", $data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal',$date);
    }

    public function deletebagian($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->m_admin->deleteBagian($id)) {
            redirect(site_url('admin/listbagian'));
    
        }
    }

    public function updatebagian()
    {
        $post = $this->input->post();
        $data = array(
            'id' => $post["id"],
            'idBagian' => $post["idBagian"],
            'Nama_Bagian' => $post["Nama_Bagian"]
        );

        $this->m_admin->updatebagian($data);
        
        redirect('admin/listbagian');
    }

    public function createbagian()
    {
        $post = $this->input->post();
        $data = array(
            'idBagian' => $post["idBagian"],
            'Nama_Bagian' => $post["Nama_Bagian"]
        );

        $this->m_admin->createbagian($data);
        
        redirect('admin/listbagian');
    }

    public function info()
    {
        $this->session->set_flashdata('info', 'This is Info Message');
    }

}