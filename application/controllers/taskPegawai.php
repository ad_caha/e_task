<?php

class taskPegawai extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('status') != "login"){
			redirect(base_url("auth"));
        }
        $this->load->database();
        $this->load->model('project_model');
    }
    public function index()
    {
        date_default_timezone_set("Asia/Bangkok");
        $listTask = $this->input->post('mytext[]');
        $idProject = $this->input->post('idProjectPegawai');
        $idPegawai = $this->input->post('idPegawai');
        foreach ($listTask as $id => $index) {
            $detailTask = $listTask[$id];
            $CreateDate = date('Y-m-d H:i:s');
            $CreateBy = $this->session->userdata("jabatan");
            $statusTask = 0;

            $data = array(
                'detailTask' => $detailTask,
                'CreateDate' => $CreateDate,
                'CreateBy' => $CreateBy,
                'statusTask' => $statusTask,
                'idProject' => $idProject
                );
            $this->project_model->addTask($data);
            var_dump($data); //for debug only
            unset($data);
        }
		redirect('taskPegawai/viewTask/'.$idPegawai.'/'.$idProject);
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["product_id" => $id])->row();
    }

    public function viewTask($idPegawai, $id = null)
    {
        if (!isset($id)) redirect('taskPegawai');

        $var["project"] = $this->project_model->getProject($id); 
        $data["project"] = $var["project"][0];
        $data["task"] = $this->project_model->getTaskByProject($id); 
        $data["id"] = $id;
        $data["idPegawai"] = $idPegawai;
        $date['date']= date('d-m-Y');
        
        for($i=0; $i< sizeof($data["task"]); $i++) {
            if ($data['task'][$i]['statusTask']=='0') {
                $data['task'][$i]['statusName'] = 'Awaiting Approval';
            } else if ($data['task'][$i]['statusTask']=='1') { 
                $data['task'][$i]['statusName'] = 'In Progress';
            } else if ($data['task'][$i]['statusTask']=='2') {
                $data['task'][$i]['statusName'] = 'Finished';
            }
        }
        

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view("dashboard/taskpegawai", $data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal',$date);
    }
    public function deleteTask($idPegawai, $idProject, $idTask=null)
    {
        if (!isset($idTask)) show_404();
        
        if ($this->project_model->deleteTask($idTask)) {
            redirect(site_url('taskPegawai/viewTask/'.$idPegawai.'/'.$idProject));
       
        }
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->project_model->deleteProject($id)) {
            redirect(site_url('listproject'));
       
        }
    }
    public function edit()
    {

        $post = $this->input->post();
        $idproject = $this->idProject = $post["idProjectPegawai"];
        $nameproject = $this->nameProject = $post["nameProjectPegawai"];
        $idPegawai = $this->input->post('idEditPegawai');

        $data = array(
            'idProject' => $idproject,
            'namaProject' => $nameproject
        );

            $this->project_model->updateProject($data);

		redirect('taskPegawai/viewTask/'.$idPegawai.'/'.$idproject);
    }
    public function editTask()
    {
        // if (!isset($id)) redirect('listproject');
       
        // $product = $this->product_model;
        // $validation = $this->form_validation;
        // $validation->set_rules($product->rules());
        $post = $this->input->post();
        $idTask = $this->idTask = $post["idTaskPegawai"];
        $detailTask = $this->detailTask = $post["detailTaskPegawai"];
        $idPegawai = $this->input->post('idEditTaskPegawai');
        $CreateBy = $this->session->userdata("jabatan");

        $data = array(
            'idTask' => $idTask,
            'detailTask' => $detailTask,
            'CreateBy' => $CreateBy
        );
        
        $dataTask = $this->project_model->getTask($idTask);
        $idProject = $dataTask[0]['idProject'];

        // if ($validation->run()) {
            $this->project_model->updateTask($data);
            // $this->session->set_flashdata('success', 'Berhasil disimpan');
        // }

		redirect('taskPegawai/viewTask/'.$idPegawai.'/'.$idProject);
    }
}
