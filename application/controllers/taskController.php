<?php

class taskController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('status') != "login"){
			redirect(base_url("auth"));
        }
        
        $this->load->database();
        $this->load->model('project_model');
    }

    public function index()
    {
        date_default_timezone_set("Asia/Bangkok");
        $listTask = $this->input->post('mytext[]');
        $idProject = $this->input->post('idProject');
        foreach ($listTask as $id => $index) {
            $detailTask = $listTask[$id];
            $CreateDate = date('Y-m-d H:i:s');
            $CreateBy = $this->session->userdata("jabatan");
            $statusTask = 0;

            $data = array(
                'detailTask' => $detailTask,
                'CreateDate' => $CreateDate,
                'CreateBy' => $CreateBy,
                'statusTask' => $statusTask,
                'idProject' => $idProject
                );
            $this->project_model->addTask($data);
            var_dump($data); //for debug only
            unset($data);
        }
		redirect('listproject/viewTask/'.$idProject);
    }
    
    public function delete($idProject, $idTask=null)
    {
        if (!isset($idTask)) show_404();
        
        if ($this->project_model->deleteTask($idTask)) {
            redirect(site_url('listproject/viewTask/'.$idProject));
       
        }
    }

    public function edit()
    {
        // if (!isset($id)) redirect('listproject');
       
        // $product = $this->product_model;
        // $validation = $this->form_validation;
        // $validation->set_rules($product->rules());
        $post = $this->input->post();
        $idTask = $this->idTask = $post["idTask"];
        $detailTask = $this->detailTask = $post["detailTask"];
        $CreateBy = $this->session->userdata("jabatan");
        $data = array(
            'idTask' => $idTask,
            'detailTask' => $detailTask,
            'CreateBy' => $CreateBy
        );
        
        $dataTask = $this->project_model->getTask($idTask);
        $idProject = $dataTask[0]['idProject'];

        // if ($validation->run()) {
            $this->project_model->updateTask($data);
            // $this->session->set_flashdata('success', 'Berhasil disimpan');
        // }

		redirect('listproject/viewTask/'.$idProject);
    }

    public function finish($idProject, $idTask=null)
    {
        if (!isset($idTask)) show_404();
        
        $data = array(
            'idTask' => $idTask,
            'statusTask' => '2'
        );

        if ($this->project_model->finishTask($data)) {
            redirect(site_url('listproject/viewTask/'.$idProject));
       
        }
    }


}