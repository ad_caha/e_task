<?php

defined('BASEPATH') or exit('No direct script access allowed');
class auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('m_login');
        $this->load->model('m_admin');
        $this->load->model('m_log_activity');
    }
    public function index()
    {
        // $this->form_validation->set_rules('name from html', 'alias', 'rules');
        $this->form_validation->set_rules('Nip', 'Nip', 'trim|required');
        $this->form_validation->set_rules('Password', 'Password', 'trim|required');
        $this->load->view('auth/login');
    }
    public function register()
    {
        $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('nip', 'Nip', 'trim|required');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['title'] = "Register";
            $this->load->view('auth/register', $data);
        } else {
            echo "data berhasil ditambah";
        }
    }
    
	function aksi_login(){
		$username = $this->input->post('Nip');
		$password = $this->input->post('Password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
			);
        $query = $this->m_login->cek_login("user",$where);
        $cek = $query->num_rows();
        foreach($query->result_array() AS $row) {
            $id = $row['id'];
            $firstname = $row['firstname'];
            $jabatan = $row['jabatan'];
            $bidang = $row['idBagian'];
            if ($row['jabatan'] == 1 || $row['jabatan'] == 2 || $row['jabatan'] == 3) {
                $admin = 1;
            } else {
                $admin = 0;
            }
        }
		if($cek > 0){
 
            $var = $this->m_admin->getJabatan($jabatan); 
            $jabatan = $var[0]['Nama_Jabatan'];
            $idJabatan = $var[0]['Id_Jabatan'];

            $bag = $this->m_admin->getBagian($bidang);
            $nama_bagian = $bag[0]['Nama_Bagian'];

			$data_session = array(
				'id' => $id,
				'username' => $username,
				'namaLengkap' => $firstname,
				'jabatan' => $jabatan,
                'bidang' => $bidang,
                'idjabatan' => $idJabatan,
                'namaBagian' => $nama_bagian,
                'status' => "login",
                'ip_address' => $this->input->ip_address(),
                'admin' => $admin
            );

			$this->session->set_userdata($data_session);
            
			$data_log = array(
				'controller' => 'auth',
				'method' => 'aksi_login',
				'activity' => 'login'
            );
            
            $this->m_log_activity->createLog($data_log);
 
			redirect(base_url("dashboard"));
 
		}else{
			$this->session->set_flashdata('danger', 'Username / Password Salah');
            $this->load->view('auth/login');
		}
	}
 
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('auth'));
	}
}
