<?php

class ProjectPegawai extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('status') != "login"){
			redirect(base_url("auth"));
        }
        $this->load->database();
        $this->load->model('project_model');
    }
    public function index($id = null)
    {
        
        if (!isset($id)) redirect('pegawai');
        // $date['date']= date('d-m-Y');
         $data['project']= $this->project_model->getProjectbyPegawai($id); 
         $data['id'] = $id;

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view('dashboard/Projectpegawai',$data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal');
    }
    public function aproval()
    {
        $idproject = $this->input->post("idProject");
        $idPegawai = $this->input->post("idPegawai");
        $namaJabatan = $this->session->userdata('jabatan');
        $aprove = '1';
        $statusProject = 'In Progress';

        $data = array(
            'idProject' => $idproject,
            'updateBy' => $namaJabatan,
            'approval' => $aprove,
            'statusProject' => $statusProject 
        );
        $this->project_model->updateAprove($data);
        $this->project_model->updateAproveTask($idproject);
		redirect('ProjectPegawai/index/'.$idPegawai);
    }

    public function addProjectshow($id){
        $data['tanggal'] = date('d-m-y');
        $data['id']=$id;
        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view('dashboard/addProjectPegawai',$data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal');
    }
    public function addProject($id)
    {
        date_default_timezone_set("Asia/Bangkok");
        $namaProject = $this->input->post('namaProject');
        $date = date('Y-m-d H:i:s');
        $updateby = $this->session->userdata("jabatan");
        $statusProject = "Tahap Awal";
        $approval = "false";
        $idPegawai = $id;

		$data = array(
            'namaProject' => $namaProject,
            'tanggalProject' => $date,
            'updateBy' => $updateby,
            'statusProject' => $statusProject,
            'approval' => $approval,
            'idPegawai' => $idPegawai
			);
		$this->project_model->addDataProject($data,'project');
		redirect('ProjectPegawai/index/'.$idPegawai);
    }
}