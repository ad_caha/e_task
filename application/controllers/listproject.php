<?php

class listproject extends CI_Controller
{    
    public function __construct()
    {
        parent::__construct();

        if($this->session->userdata('status') != "login"){
			redirect(base_url("auth"));
        }
        
        $this->load->database();
        $this->load->model('project_model');
        $this->load->helper(array('form', 'url'));
		$this->load->helper(array('url','download'));	
    }

    public function index()
    {
        $date['date']= date('d-m-Y');
        $data['project']= $this->project_model->getProjectByUserId($this->session->userdata('id')); 
        
        // Get List Project
        for($i=0; $i< sizeof($data["project"]); $i++) {
            $task = $this->project_model->getTaskByProject($data["project"][$i]['idProject']);
            $finishedTask = 0;
            for($j=0; $j< sizeof($task); $j++) {
                if ($task[$j]['statusTask']==2) {
                    $finishedTask++;
                }
            }
            if (sizeof($task)==0) {
                $data["project"][$i]['progressTask'] =  0;
            } else {
                $data["project"][$i]['progressTask'] =  round($finishedTask/sizeof($task)*100);
            }
        }

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view('dashboard/listproject',$data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal',$date);
        $this->load->view('dashboard/shared/modalFinishProject');
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["product_id" => $id])->row();
    }

    public function viewTask($id = null)
    {
        if (!isset($id)) redirect('listproject');

        $var["project"] = $this->project_model->getProject($id); 
        $data["project"] = $var["project"][0];
        $data["task"] = $this->project_model->getTaskByProject($id); 
        $data['id'] = $id;
        // if (!$data["product"]) show_404();
        
        for($i=0; $i< sizeof($data["task"]); $i++) {
            if ($data['task'][$i]['statusTask']=='0') {
                $data['task'][$i]['statusName'] = 'Awaiting Approval';
            } else if ($data['task'][$i]['statusTask']=='1') { 
                $data['task'][$i]['statusName'] = 'In Progress';
            } else if ($data['task'][$i]['statusTask']=='2') {
                $data['task'][$i]['statusName'] = 'Finished';
            }
        }
        
        $date['date']= date('d-m-Y');

        $this->load->view('dashboard/shared/header');
        $this->load->view('dashboard/shared/sidebar');
        $this->load->view('dashboard/shared/topbar');
        $this->load->view("dashboard/listtask", $data);
        $this->load->view('dashboard/shared/footer');
        $this->load->view('dashboard/shared/modal',$date);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->project_model->deleteProject($id)) {
            redirect(site_url('listproject'));
       
        }
    }
    public function edit()
    {
        // if (!isset($id)) redirect('listproject');
       
        // $product = $this->product_model;
        // $validation = $this->form_validation;
        // $validation->set_rules($product->rules());
        $post = $this->input->post();
        $idproject = $this->idProject = $post["idProject"];
        $nameproject = $this->nameProject = $post["nameProject"];

        $data = array(
            'idProject' => $idproject,
            'namaProject' => $nameproject
        );

        // if ($validation->run()) {
            $this->project_model->updateProject($data);
            // $this->session->set_flashdata('success', 'Berhasil disimpan');
        // }

        // $date['date']= date('d-m-Y');
        // $data['project']= $this->project_model->getAllProject(); 

        // $this->load->view('dashboard/shared/header');
        // $this->load->view('dashboard/shared/sidebar');
        // $this->load->view('dashboard/listproject',$data);
        // $this->load->view('dashboard/shared/footer');
        // $this->load->view('dashboard/shared/modal',$date);
        
		redirect('listproject/viewTask/'.$idproject);
    }
    
	public function aksi_upload(){

        $post = $this->input->post();
        $idProject = $post["finishProjectId"];
        $pathDoc = '';
        if ($post["filenameProject"]!='') {
            $path = './upload/'.$idProject.'/';
            if (!is_dir('upload/'.$idProject)) {
                mkdir('./upload/'.$idProject.'/', 0777, TRUE);
            }
    
            $config['upload_path']          = $path;
            $config['allowed_types']        = '*';
            // $config['max_size']             = 100;
            // $config['max_width']            = 1024;
            // $config['max_height']           = 768;
     
            $this->load->library('upload', $config);
            
            if ( ! $this->upload->do_upload('berkas')){
                $error = array('error' => $this->upload->display_errors());
                // redirect(site_url('listproject'));
            }else{
                $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                $pathDoc = $idProject.'/'.$upload_data['file_name'];
                // redirect(site_url('listproject'));
            }
        }
        
        $data = array(
            'idProject' => $idProject,
            'pathDoc' => $pathDoc,
            'statusProject' => 'Finished'
        );

        $this->project_model->updateProject($data);
        redirect(site_url('listproject'));
    }
    
	public function download($id, $path){				
		force_download('upload/'.$id.'/'.$path,NULL);
	}	
}
