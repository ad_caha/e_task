<?php

class project_model extends CI_model {

    public function getAllProject()
    {
        $query = $this->db->get('project');
        return $query->result_array();
    }
    public function addDataProject($data, $table){
        $this->db->insert($table,$data);
    }

    public function getTop5()
    {
        $this->db->order_by('tanggalProject','desc');
        $this->db->limit(5);
        $query = $this->db->get('project');
        return $query->result_array();
    }

    public function getProject($id)
    {
        $this->db->where('idProject', $id);
        $query = $this->db->get('project');
        return $query->result_array();
    }
    public function getProjectbyPegawai($id)
    {
        $sql = "SELECT a.*, j.Nama_Jabatan FROM `project` as a left join user u on a.idPegawai = u.id left join jabatan as j on u.jabatan = j.id WHERE a.idPegawai = ?";
        $query = $this->db->query($sql, array($id));
        // $this->db->where('idPegawai', $id);
        // $query = $this->db->get('project');
        return $query->result_array();
    }

    public function getTop5ByUserId($id)
    {
        $this->db->where('idPegawai', $id);
        $this->db->order_by('tanggalProject','desc');
        $this->db->limit(5);
        $query = $this->db->get('project');
        return $query->result_array();
    }

    public function getProjectByUserId($id)
    {
        $sql = "SELECT a.*, j.Nama_Jabatan FROM `project` as a left join user u on a.idPegawai = u.id left join jabatan as j on u.jabatan = j.id WHERE a.idPegawai = ?";
        $query = $this->db->query($sql, array($id));
        // $this->db->where('idPegawai', $id);
        // $query = $this->db->get('project');
        return $query->result_array();
    }

    public function getTaskByProject($id)
    {
        $this->db->where('idProject', $id);
        $query = $this->db->get('task');
        return $query->result_array();
    }

    public function deleteProject($id)
    {
        return $this->db->delete('project', array("idProject" => $id));
    }

    public function updateProject($data)
    {
        $this->db->update('project', $data, array('idProject' => $data['idProject']));
    }
    public function updateAprove($data)
    {
        $this->db->update('project', $data, array('idProject' => $data['idProject']));
    }

    public function updateAproveTask($id)
    {
        $this->db->update('task', array("statusTask" => "1"), array('idProject' => $id));
    }

    public function addTask($data) {
        $this->db->insert('task', $data);
        if ($this->db->affected_rows() == '1') return TRUE;
        return FALSE;    //error ocures
    }
    
    public function deleteTask($id)
    {
        return $this->db->delete('task', array("idTask" => $id));
    }

    public function updateTask($data)
    {
        $this->db->update('task', $data, array('idTask' => $data['idTask']));
    }

    public function finishTask($data)
    {
        return $this->db->update('task', $data, array('idTask' => $data['idTask']));
    }

    public function getTask($id)
    {
        $this->db->where('idTask', $id);
        $query = $this->db->get('task');
        return $query->result_array();
    }

    public function getCountProject($id)
    {
        $this->db->where('idPegawai', $id);
        $query = $this->db->get('project');
        return $query->num_rows();
    }

    public function getCountTask($id)
    {
        $sql = "SELECT a.* FROM `project` as a left join task t on a.idProject = t.idProject WHERE a.idPegawai = ?";
        $query = $this->db->query($sql, array($id));
        return $query->num_rows();
    }



}