<?php

class pegawai_model extends CI_model {
    public function getAllPegawai()
    {
        $query = $this->db->get('user');
        return $query->result_array();
    }
    public function getAllPegawaiByBidang($idJabatan,$idBidang,$table)
    {
        $query = $this->db->get_where( $table, array('jabatan' => $idJabatan , 'idBagian'=> $idBidang ));
        return $query->result_array();
    }
}