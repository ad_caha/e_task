<?php 

class m_log_activity extends CI_Model{	

	function createLog($data){
		$dataLog = array(
            'user_id' => $this->session->userdata("id"),
            'module' => $data['module'],
            'controller' => $data['controller'],
            'method' => $data['method'],
            'activity' => $data['activity'],
            'ip_address' => $this->session->userdata("ip_address")
		);
        $this->db->insert('log_activity',$dataLog);
	}	
}