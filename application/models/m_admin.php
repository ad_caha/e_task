<?php 

class m_admin extends CI_Model{	
	
    public function getCountUser()
    {
        $query = $this->db->get('user');
        return $query->num_rows();
    }
	
    public function getCountJabatan()
    {
        $query = $this->db->get('jabatan');
        return $query->num_rows();
    }
	
    public function getCountBagian()
    {
        $query = $this->db->get('bagian');
        return $query->num_rows();
    }
	
    public function getAllUsers()
    {
        $sql = "SELECT a.*, j.Nama_Jabatan, b.Nama_Bagian FROM `user` as a left join bagian b on a.idBagian = b.id left join jabatan as j on a.jabatan = j.id";
        $query = $this->db->query($sql);
        // $query = $this->db->get('user');
        return $query->result_array();
	}

    public function getAllJabatan()
    {
        $query = $this->db->get('jabatan');
        return $query->result_array();
	}

    public function getAllBagian()
    {        
        $query = $this->db->get('bagian');
        return $query->result_array();
	}
	
    public function getUser($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('user');
        return $query->result_array();
    }
	
    public function deleteUser($id)
    {
        return $this->db->delete('user', array("id" => $id));
	}
	
    public function updateuser($data)
    {
        $this->db->update('user', $data, array('id' => $data['id']));
	}
	
    public function createuser($data)
    {
        $this->db->insert('user', $data);
	}
	
    public function getJabatan($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('jabatan');
        return $query->result_array();
	}
	
    public function deleteJabatan($id)
    {
        return $this->db->delete('jabatan', array("id" => $id));
	}
	
    public function updatejabatan($data)
    {
        $this->db->update('jabatan', $data, array('id' => $data['id']));
	}
	
    public function createjabatan($data)
    {
        $this->db->insert('jabatan', $data);
	}
	
    public function getBagian($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('bagian');
        return $query->result_array();
	}
	
    public function deleteBagian($id)
    {
        return $this->db->delete('bagian', array("id" => $id));
	}
	
    public function updatebagian($data)
    {
        $this->db->update('bagian', $data, array('id' => $data['id']));
	}
	
    public function createbagian($data)
    {
        $this->db->insert('bagian', $data);
	}

}