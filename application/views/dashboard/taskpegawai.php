        <!-- Begin Page Content -->
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" defer></script>
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.css">
        <div class="container-fluid">

            <!-- Page Heading -->
                <a href="<?php echo base_url().'ProjectPegawai/index/'.$idPegawai  ?>">Back</a>
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">List Task Project  :   <?= $project['namaProject']?>
                <a href="#" data-id="<?= $project['idProject']?>" data-name="<?= $project['namaProject']?>" style="width:70px" data-toggle="modal" data-target="#editProjectPegawaiModal"><i class="fas fa-pencil-alt" style="font-size: 50%; top:-.5em;position: relative;"></i>
                </a></h1>

                
                <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#taskModalPegawai" data-id="<?= $project['idProject']?>"><i class="fas fa-download fa-sm text-white-50"></i> Tambah Task </a> 
            </div>

                
            <!-- Content Row -->

            <div class="row">
                <!-- Area Chart -->
                <div class="col-xl-12 col-lg-12">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Detail Task</th>
                            <th>Tanggal Task</th>
                            <th>Status Task</th>
                            <th>Update By</th>
                            <th width=250px style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                        $i=1;
                        foreach($task as $row)
                        { ?>
                        <tr>
                            <td></td>
                            <td><?= $row['detailTask']?></td>
                            <td><?= $row['CreateDate']?></td>
                            <td><?= $row['statusName']?></td>
                            <td><?= $row['CreateBy']?></td>
                            <td style="text-align: center;">
                                <button type="button" class="btn btn-info" data-id="<?= $row['idTask']?>" data-name="<?= $row['detailTask']?>" style="width:100px" data-toggle="modal" data-target="#editTaskModalPegawai"><i class="fas fa-edit"></i> Edit</button>
                                <button type="button" class="btn btn-danger" style="width:100px" onclick="deleteConfirm('<?php echo site_url('taskPegawai/deleteTask/'.$idPegawai.'/'.$project['idProject'].'/'.$row['idTask']) ?>')"><i class="fas fa-trash"></i> Delete</button>
                            </td>
                        </tr>
            <?php } ?>
        </tbody>
    </table>
                    
                </div>
            </div>


        </div>
        <!-- /.container-fluid -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
<script type="text/javascript">
   $(document).ready(function() {
    var t = $('#example').DataTable({
        "scrollX" : true,
        "order": [[ 2, "desc" ]],
        "columnDefs": [ {
            "targets": 6,
            "orderable": false
        } ],
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ]
    });
    
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $('#taskModalPegawai').on('show.bs.modal', function(e) {
        
        var projectId = $(e.relatedTarget).data('id');
        $(this).find("#idProjectPegawai").val(projectId);  
        $(this).find("#idPegawai").val(<?= $idPegawai ?>);  
    })

    $('#editTaskModalPegawai').on('show.bs.modal', function(e) {
        var idTask = $(e.relatedTarget).data('id');
        var detailTask = $(e.relatedTarget).data('name');
        $(this).find("input[type=text]").val(detailTask);
        $(this).find("#idTaskPegawai").val(idTask);    
        $(this).find("#idEditTaskPegawai").val(<?= $idPegawai ?>);           
    })

    $('#editProjectPegawaiModal').on('show.bs.modal', function(e) {
        var projectId = $(e.relatedTarget).data('id');
        var projectName = $(e.relatedTarget).data('name');
        $(this).find("input[type=text]").val(projectName);
        $(this).find("#projectIdPegawai").val(projectId);    
        $(this).find("#idEditPegawai").val(<?= $idPegawai ?>);    
    })
} );
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>
      