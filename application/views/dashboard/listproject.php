        <!-- Begin Page Content -->
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" defer></script>
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.css">
        <div class="container-fluid">

            <!-- Page Heading -->
                <a href="<?php echo base_url().'dashboard' ?>">Back</a>
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">List Project</h1>
            </div>

                
            <!-- Content Row -->

            <div class="row">
                <!-- Area Chart -->
                <div class="col-xl-12 col-lg-12">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nama Project</th>
                            <th>Tanggal Project</th>
                            <th>Status Project</th>
                            <th>Update Terakhir</th>
                            <!-- <th>Ubah Status</th> -->
                            <th width=250px style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                        $i=1;
                        foreach($project as $row)
                        { ?>
                        <tr>
                            <td></td>
                            <td><?= $row['namaProject']?></td>
                            <td><?= $row['tanggalProject']?></td>
                            <td><?= $row['statusProject']?> 
                            <?php if($row['statusProject']== "In Progress" ) {?>
                            (<?= $row['progressTask']?>%)
                            <?php } ?>
                            </td>
                            <td><?= $row['Nama_Jabatan']?></td>
                            <td style="text-align: center; width:250px">
                                <?php if($row['statusProject']== "In Progress" ) {?>
                                    <button type="button" 
                                    <?php if($row['progressTask']!= 100 ) {?>
                                    disabled
                                    <?php } ?>
                                    class="btn btn-warning" style="width:140px" data-id="<?= $row['idProject']?>" data-toggle="modal" data-target="#finishProjectModal" ><i class="fas fa-check-square"></i> Finish Project</button>
                                <?php } else if($row['pathDoc']!= '' ) { ?><button type="button" 
                                    <?php if($row['pathDoc']== '' ) {?>
                                    disabled
                                    <?php } ?>
                                    class="btn btn-primary" style="width:140px" onclick="location.href='<?php echo base_url();?>listproject/download/<?= $row['pathDoc']?>'" ><i class="fas fa-file-download"></i> Download Doc</button>
                                <?php } ?>
                                <button type="button" class="btn btn-info" style="width:100px" onclick="location.href='<?php echo base_url();?>listproject/viewTask/<?= $row['idProject']?>'"><i class="fas fa-eye"></i> View Task</button>
                                <?php if($row['statusProject']== "Tahap Awal" ) {?>
                                <button type="button" class="btn btn-danger" style="width:70px" onclick="deleteConfirm('<?php echo site_url('listproject/delete/'.$row['idProject']) ?>')"><i class="fas fa-trash"></i> Delete</button>
                                <?php } ?>
                                </td>
                        </tr>
            <?php } ?>
        </tbody>
    </table>
                    
                </div>  
            </div>


        </div>
        <!-- /.container-fluid -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
<script type="text/javascript">
   $(document).ready(function() {
    var t = $('#example').DataTable({
        "scrollX": true,
        "order": [[ 2, "desc" ]],
        "columnDefs": [ {
            "targets": 5,
            "orderable": false
        } ],
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ]
    });
    
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $('#editProjectModal').on('show.bs.modal', function(e) {
        var projectId = $(e.relatedTarget).data('id');
        var projectName = $(e.relatedTarget).data('name');
        $(this).find("input[type=text]").val(projectName);
        $(this).find("#projectId").val(projectId);        
    })

    $('#finishProjectModal').on('show.bs.modal', function(e) {
        var projectId = $(e.relatedTarget).data('id');
        $(this).find("#finishProjectId").val(projectId);        
    })

} );
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
    function getFileData(myFile){
        var file = myFile.files[0]; 
        var filename = file.name;
        $("#filenameProject").val(filename);    
    }
</script>
      