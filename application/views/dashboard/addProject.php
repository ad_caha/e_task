        <!-- CONTENT -->



        <div class="col-xl-8 col-lg-12 offset-xl-2">
                    <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">Tambah Project</h6>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                            <!-- list Project -->
                            <form action="<?php echo base_url().'project/addProject'; ?>" method="POST">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Project</label>
                                    <input type="text" class="form-control" id="exampleInputEmail1" name="namaProject" aria-describedby="" required="true">
                                    <small id="emailHelp" class="form-text text-muted">Tanggal : <?= $tanggal;?></small>
                            </div>
                            <button type="submit"  class="btn btn-primary my-3 pull-right">Konfirmasi</button>
                            </form>
                            <!-- End List Project -->
                        </div>
                    </div>
                </div>




        <!-- End CONTENT -->

        </div>
     </div>
        <!-- /.container-fluid -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
