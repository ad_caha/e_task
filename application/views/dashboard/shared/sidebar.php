    <?php if ($this->session->userdata("idjabatan") != 'St') { ?>

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('/dashboard') ?>">
            <div class="sidebar-brand-text mx-3">
                <h4>E-Task UT</h4>
            </div>
            <div class="sidebar-brand-text-invert mx-3">
                <h4>UT</h4>
            </div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="<?= base_url('/dashboard') ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <?php //if ($this->session->userdata("admin")) { ?>
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Admin
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('/admin') ?>" aria-expanded="false">
                <i class="fas fa-fw fa-user-cog"></i>
                <span>Pengaturan Admin</span>
            </a>
        </li>
        <?php// } ?>

        <?php if ($this->session->userdata("idjabatan") != 'St') { ?>
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Pegawai
        </div>

        <!-- Nav Item - Pages Collapse Menu -->

        <!-- Nav Item - Utilities Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('/pegawai') ?>" aria-expanded="false">
                <i class="fas fa-tasks"></i>
                <span>Task Pegawai</span>
            </a>
        </li>
        <?php } ?>


        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block pb-5">

        <div data-provide="calendar"></div>

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline pt-3">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
        <?php } ?>
    <!-- End of Sidebar -->