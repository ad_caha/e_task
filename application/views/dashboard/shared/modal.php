  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="<?php echo base_url().'auth/logout'; ?>">Logout</a>
                    </div>
                </div>
            </div>
        </div>

  <!-- Success Modal -->
  <div class="modal fade" id="success_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-gradient-success">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Project telah di simpan</div>
                    <div class="modal-footer">
                        <button class="btn btn-success" type="submit" data-dismiss="modal">Simpan</button>
                    </div>
                </div>
            </div>
        </div>


  <!-- Modal Insert -->
        <div class="modal fade" id="projectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Project  </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <form action="<?php echo base_url().'dashboard/addProject'; ?>" method="post">
                    <div class="modal-body">
                        <div class="col-md-12">
                            <span class="float-left"><h6>Name Project</h6> </span> <span class="float-right"><?= $date ?></span>
                        </div>
                        <div class="input_fields_wrap">
                            <div><input type="text" class="form-control bg-light border-0 small" name="mytext[0]" aria-label="Search" aria-describedby="basic-addon2"></div>
                            <button class="add_field_button btn btn-light mt-2 mb-2">Add Task</button>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="editProjectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Project  </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form action="<?php echo base_url().'listproject/edit' ?>" method="post" enctype="multipart/form-data">
                        <div class="input_fields_wrap">
                                <input type="hidden" name="idProject" id="projectId" value="" />
                                <div class="form-group">
                                    <label for="name">Name Project</label>
                                    <input class="form-control"
                                    type="text" name="nameProject" placeholder="Project name" value="" />
                                    <div class="invalid-feedback">
                                    </div>
                                </div>
                        </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <input class="btn btn-success" type="submit" name="btn" value="Save" />
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="editProjectPegawaiModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Project  </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form action="<?php echo base_url().'taskPegawai/edit' ?>" method="post" enctype="multipart/form-data">
                        <div class="input_fields_wrap">
                                <input type="hidden" name="idProjectPegawai" id="projectIdPegawai" value="" />
                                <input type="hidden" name="idEditPegawai" id="idEditPegawai" value="" />
                                <div class="form-group">
                                    <label for="name">Name Project</label>
                                    <input class="form-control"
                                    type="text" name="nameProjectPegawai" placeholder="Project name" value="" />
                                    <div class="invalid-feedback">
                                    </div>
                                </div>
                        </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <input class="btn btn-success" type="submit" name="btn" value="Save" />
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="taskModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="<?php echo base_url().'taskController' ?>" method="post" enctype="multipart/form-data">
                        <div class="modal-header">
                            <input type="hidden" name="idProject" id="idProject" value="" />
                            <h5 class="modal-title" id="exampleModalLabel">Add Task  </h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        <div class="input_fields_wrap">
                            <button class="add_field_button btn btn-light mt-2 mb-2">Add More Task</button>
                        </div>

                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <input class="btn btn-success" type="submit" name="btn" value="Create" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="taskModalPegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="<?php echo base_url().'taskPegawai'?>" method="post" enctype="multipart/form-data">
                        <div class="modal-header">
                            <input type="hidden" name="idProjectPegawai" id="idProjectPegawai" value="" />
                            <input type="hidden" name="idPegawai" id="idPegawai" value="" />
                            <h5 class="modal-title" id="exampleModalLabel">Add Task  </h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        <div class="input_fields_wrap">
                            <button class="add_field_button btn btn-light mt-2 mb-2">Add More Task</button>
                        </div>

                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <input class="btn btn-success" type="submit" name="btn" value="Create" />
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Delete Confirmation-->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
                </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="approvalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Project yang di approve tidak bisa dikembalikan</div>
                <div class="modal-footer">
                </div>
                </div>
            </div>
        </div>

        <!-- Edit Task Modal -->
        <div class="modal fade" id="editTaskModalPegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Task  </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form action="<?php echo base_url().'taskPegawai/editTask' ?>" method="post" enctype="multipart/form-data">
                        <div class="input_fields_wrap">
                                <input type="hidden" name="idTaskPegawai" id="idTaskPegawai" value="" />
                                <input type="hidden" name="idEditTaskPegawai" id="idEditTaskPegawai" value="" />
                                <div class="form-group">
                                    <label for="name">Nama Task</label>
                                    <input class="form-control"
                                    type="text" name="detailTaskPegawai" placeholder="Task name" value="" />
                                    <div class="invalid-feedback">
                                    </div>
                                </div>
                        </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <input class="btn btn-success" type="submit" name="btn" value="Save" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editTaskModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Task  </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form action="<?php echo base_url().'taskController/edit' ?>" method="post" enctype="multipart/form-data">
                        <div class="input_fields_wrap">
                                <input type="hidden" name="idTask" id="idTask" value="" />
                                <div class="form-group">
                                    <label for="name">Nama Task</label>
                                    <input class="form-control"
                                    type="text" name="detailTask" placeholder="Task name" value="" />
                                    <div class="invalid-feedback">
                                    </div>
                                </div>
                        </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <input class="btn btn-success" type="submit" name="btn" value="Save" />
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Finish Task Confirmation-->
        <div class="modal fade" id="finishTaskModal" tabindex="-2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pekerjaan sudah selesai?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Status pekerjaan yang sudah diselesaikan tidak bisa diubah.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a id="btn-finish" class="btn btn-success" href="#">Finish</a>
                </div>
                </div>
            </div>
        </div>