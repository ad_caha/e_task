
        <!-- Finish Project -->
        <div class="modal fade" id="finishProjectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <?php echo form_open_multipart('listproject/aksi_upload');?>
                        <div class="modal-header">
                            <input type="hidden" name="finishProjectId" id="finishProjectId" value="" />
                            <input type="hidden" name="filenameProject" id="filenameProject" value="" />
                            <h5 class="modal-title" id="exampleModalLabel">Finish Project  </h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                        Upload Document
                        <div class="input_fields_wrap">
                        </div>
                        
                        <input type="file" name="berkas" onchange="getFileData(this);"/>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <input class="btn btn-success" type="submit" name="btn" value="Finish" />
                        </div>
                    </form>
                </div>
            </div>
        </div>