        <!-- Begin Page Content -->
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" defer></script>
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.css">
        <div class="container-fluid">

            <!-- Page Heading -->
                <a href="<?php echo base_url().'listproject' ?>">Back</a>
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">List Task Project  :   <?= $project['namaProject']?>
                <a href="#" data-id="<?= $project['idProject']?>" data-name="<?= $project['namaProject']?>" style="width:70px" data-toggle="modal" data-target="#editProjectModal"><i class="fas fa-pencil-alt" style="font-size: 50%; top:-.5em;position: relative;"></i>
                </a></h1>
                <?php if($project['statusProject']== "Tahap Awal" ) {?>
                <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#taskModal" data-id="<?= $project['idProject']?>"><i class="fas fa-download fa-sm text-white-50"></i> Tambah Task </a> 
                <?php } ?>
            </div>

                
            <!-- Content Row -->

            <div class="row">
                <!-- Area Chart -->
                <div class="col-xl-12 col-lg-12">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Detail Task</th>
                            <th>Tanggal Task</th>
                            <th>Status Task</th>
                            <th>Dibuat Oleh</th>
                            <!-- <th>Approval</th> -->
                            <th width=250px style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                        $i=1;
                        foreach($task as $row)
                        { ?>
                        <tr height="55px">
                            <td></td>
                            <td><?= $row['detailTask']?></td>
                            <td><?= $row['CreateDate']?></td>
                            <td><?= $row['statusName']?></td>
                            <td><?= $row['CreateBy']?></td>
                            
                            <td style="text-align: center;">
                            <?php if ($row['statusTask']=='0') { ?>
                            <button type="button" class="btn btn-info" data-id="<?= $row['idTask']?>" data-name="<?= $row['detailTask']?>" style="width:70px" data-toggle="modal" data-target="#editTaskModal"><i class="fas fa-edit"></i>Edit</button>
                            <button type="button" class="btn btn-danger" style="width:70px" onclick="deleteConfirm('<?php echo site_url('taskController/delete/'.$project['idProject'].'/'.$row['idTask']) ?>')"><i class="fas fa-trash"></i> Delete</button>
                            <?php } else if ($row['statusTask']=='1') { ?>
                            <button type="button" class="btn btn-warning" style="width:140px" onclick="finishConfirm('<?php echo site_url('taskController/finish/'.$project['idProject'].'/'.$row['idTask']) ?>')"><i class="fas fa-check-square"></i> Finish Task</button>
                            <?php } else { ?>
                            <?php } ?>
                            </td>
                        </tr>
            <?php } ?>
        </tbody>
    </table>
                    
                </div>
            </div>


        </div>
        <!-- /.container-fluid -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
<script type="text/javascript">
   $(document).ready(function() {
    var t = $('#example').DataTable({
        "scrollX" : true,
        "order": [[ 2, "desc" ]],
        "columnDefs": [ {
            "targets": 5,
            "orderable": false
        } ],
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ]
    });
    
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $('#taskModal').on('show.bs.modal', function(e) {
        var projectId = $(e.relatedTarget).data('id');
        $(this).find("#idProject").val(projectId);  
    })

    $('#editTaskModal').on('show.bs.modal', function(e) {
        var idTask = $(e.relatedTarget).data('id');
        var detailTask = $(e.relatedTarget).data('name');
        $(this).find("input[type=text]").val(detailTask);
        $(this).find("#idTask").val(idTask);        
    })

    $('#editProjectModal').on('show.bs.modal', function(e) {
        var projectId = $(e.relatedTarget).data('id');
        var projectName = $(e.relatedTarget).data('name');
        $(this).find("input[type=text]").val(projectName);
        $(this).find("#projectId").val(projectId);        
    })
} );
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }

    function finishConfirm(url){
        $('#btn-finish').attr('href', url);
        $('#finishTaskModal').modal();
    }
</script>
      