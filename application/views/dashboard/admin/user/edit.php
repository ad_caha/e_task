        <!-- CONTENT -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <!-- Content Row -->
            <!-- <div class="modal-content"> -->
                <a href="<?php echo base_url().'admin/listuser' ?>">Back</a>
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit User  </h5>
                </div>
                <div class="modal-body">
                <form action="<?php echo base_url().'admin/updateuser' ?>" method="post" enctype="multipart/form-data">
                    <div class="input_fields_wrap">
                            <input type="hidden" name="id" id="id" value="<?= $user['id']?>" />
                            <div class="form-group">
                                <label for="name">Username</label>
                                <input class="form-control"
                                type="text" name="username" placeholder="Username" value="<?= $user['username']?>" readonly />
                                <div class="invalid-feedback">
                                </div>
                            </div>
                    </div>
                    <div class="input_fields_wrap">
                            <div class="form-group">
                                <label for="name">First Name</label>
                                <input class="form-control"
                                type="text" name="firstname" placeholder="Firstname" value="<?= $user['firstname']?>" />
                                <div class="invalid-feedback">
                                </div>
                            </div>
                    </div>
                    <div class="input_fields_wrap">
                            <div class="form-group">
                                <label for="name">NIP</label>
                                <input class="form-control"
                                type="text" name="NIP" placeholder="NIP" value="<?= $user['NIP']?>" />
                                <div class="invalid-feedback">
                                </div>
                            </div>
                    </div>
                    <div class="input_fields_wrap">
                            <div class="form-group">
                                <label for="name">Email</label>
                                <input class="form-control"
                                type="text" name="email" placeholder="Email" value="<?= $user['email']?>" />
                                <div class="invalid-feedback">
                                </div>
                            </div>
                    </div>
                    <div class="input_fields_wrap">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Jabatan</label>
                            <select class="form-control" id="jabatan" name="jabatan">
                            <option value="">No Selected</option>
                            <?php
                            foreach($jabatan as $row)
                            { ?>
                            <option <?php if ($user['jabatan'] == $row['id']) { ?> selected <?php } ?> value="<?= $row['id']?>"><?= $row['Nama_Jabatan']?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="input_fields_wrap">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Bagian</label>
                            <select class="form-control" id="bagian" name="bagian">
                            <option value="">No Selected</option>
                            <?php
                            foreach($bagian as $row)
                            { ?>
                            <option <?php if ($user['idBagian'] == $row['id']) { ?> selected <?php } ?> value="<?= $row['id']?>"><?= $row['Nama_Bagian']?></option>
                            <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal" onclick="window.location.href='<?php echo base_url().'admin/listuser' ?>'">Cancel</button>
                        <input class="btn btn-success" type="submit" name="btn" value="Save" />
                    </div>
                </form>
            <!-- </div> -->
            
        </div>



        <!-- End CONTENT -->

     </div>
        <!-- /.container-fluid -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
