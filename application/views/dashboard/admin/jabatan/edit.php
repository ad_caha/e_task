        <!-- CONTENT -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <!-- Content Row -->
            <!-- <div class="modal-content"> -->
                <a href="<?php echo base_url().'admin/listjabatan' ?>">Back</a>
                <a href="<?php echo base_url().'admin/info' ?>">Info</a>
                <?php if ($this->session->flashdata('info')): ?>
                <div class="alert alert-info" role="alert">
                    <?php echo $this->session->flashdata('info'); ?>
                </div>
                <?php endif; ?>
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Jabatan  </h5>
                </div>
                <div class="modal-body">
                <form action="<?php echo base_url().'admin/updatejabatan' ?>" method="post" enctype="multipart/form-data">
                    <div class="input_fields_wrap">
                            <input type="hidden" name="id" id="id" value="<?= $jabatan['id']?>" />
                            <div class="form-group">
                                <label for="name">Kode Jabatan</label>
                                <input class="form-control"
                                type="text" name="Id_Jabatan" placeholder="Kode Jabatan" value="<?= $jabatan['Id_Jabatan']?>" readonly />
                                <div class="invalid-feedback">
                                </div>
                            </div>
                    </div>
                    <div class="input_fields_wrap">
                            <div class="form-group">
                                <label for="name">Nama Jabatan</label>
                                <input class="form-control"
                                type="text" name="Nama_Jabatan" placeholder="Nama Jabatan" value="<?= $jabatan['Nama_Jabatan']?>" />
                                <div class="invalid-feedback">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal" onclick="window.location.href='<?php echo base_url().'admin/listjabatan' ?>'">Cancel</button>
                        <input class="btn btn-success" type="submit" name="btn" value="Save" />
                    </div>
                </form>
            <!-- </div> -->
            
        </div>



        <!-- End CONTENT -->

     </div>
        <!-- /.container-fluid -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
