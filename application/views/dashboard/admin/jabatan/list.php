        <!-- Begin Page Content -->
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" defer></script>
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.css">
        <div class="container-fluid">

            <!-- Page Heading -->
                <a href="<?php echo base_url().'admin' ?>">Back</a>
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Pengaturan Jabatan</h1>
                <a href="<?php echo site_url('admin/addjabatan') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Tambah Jabatan </a>
            </div>

                
            <!-- Content Row -->

            <div class="row">
                <!-- Area Chart -->
                <div class="col-xl-12 col-lg-12">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Kode Jabatan</th>
                            <th>Nama Jabatan</th>
                            <th width=250px style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                        $i=1;
                        foreach($jabatan as $row)
                        { ?>
                        <tr>
                            <td></td>
                            <td><?= $row['id']?></td>
                            <td><?= $row['Id_Jabatan']?></td>
                            <td><?= $row['Nama_Jabatan']?></td>
                            <td style="text-align: center;">
                                <button type="button" class="btn btn-info" data-id="" data-name="" style="width:70px" onclick="window.location.href='<?php echo base_url().'admin/editjabatan/'.$row['id'] ?>'"><i class="fas fa-edit"></i> Edit</button>
                                <button type="button" class="btn btn-danger" style="width:70px" onclick="deleteConfirm('<?php echo site_url('admin/deletejabatan/'.$row['id']) ?>')"><i class="fas fa-trash"></i> Delete</button>
                            </td>
                        </tr>
            <?php } ?>
        </tbody>
    </table>
                    
                </div>  
            </div>


        </div>
        <!-- /.container-fluid -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
<script type="text/javascript">
   $(document).ready(function() {
    var t = $('#example').DataTable({
        "order": [[ 1, "asc" ]],
        "columnDefs": [ {
            "targets": 6,
            "orderable": false
        } ],
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ]
    });
    
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $('#editProjectModal').on('show.bs.modal', function(e) {
        var projectId = $(e.relatedTarget).data('id');
        var projectName = $(e.relatedTarget).data('name');
        $(this).find("input[type=text]").val(projectName);
        $(this).find("#projectId").val(projectId);        
    })

} );
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>
      