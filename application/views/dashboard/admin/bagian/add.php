        <!-- CONTENT -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <!-- Content Row -->
            <!-- <div class="modal-content"> -->
                <a href="<?php echo base_url().'admin/listbagian' ?>">Back</a>
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Bagian  </h5>
                </div>
                <div class="modal-body">
                <form action="<?php echo base_url().'admin/createbagian' ?>" method="post" enctype="multipart/form-data">
                    <div class="input_fields_wrap">
                            <input type="hidden" name="id" id="id" value="" />
                            <div class="form-group">
                                <label for="name">Kode Bagian</label>
                                <input class="form-control"
                                type="text" name="idBagian" placeholder="Kode Bagian" value="" />
                                <div class="invalid-feedback">
                                </div>
                            </div>
                    </div>
                    <div class="input_fields_wrap">
                            <div class="form-group">
                                <label for="name">Nama Bagian</label>
                                <input class="form-control"
                                type="text" name="Nama_Bagian" placeholder="Nama Bagian" value="" />
                                <div class="invalid-feedback">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal" onclick="window.location.href='<?php echo base_url().'admin/listbagian' ?>'">Cancel</button>
                        <input class="btn btn-success" type="submit" name="btn" value="Save" />
                    </div>
                </form>
            <!-- </div> -->
            
        </div>



        <!-- End CONTENT -->

     </div>
        <!-- /.container-fluid -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
