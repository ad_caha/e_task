  
        <!-- Edit User Admin Modal -->
        <div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit User  </h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form action="<?php echo base_url().'taskController/edit' ?>" method="post" enctype="multipart/form-data">
                        <div class="input_fields_wrap">
                                <input type="hidden" name="idTask" id="idTask" value="" />
                                <div class="form-group">
                                    <label for="name">Nama User</label>
                                    <input class="form-control"
                                    type="text" name="detailTask" placeholder="Task name" value="" />
                                    <div class="invalid-feedback">
                                    </div>
                                </div>
                        </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <input class="btn btn-success" type="submit" name="btn" value="Save" />
                        </div>
                    </form>
                </div>
            </div>
</div>