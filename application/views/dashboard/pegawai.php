        <!-- Begin Page Content -->
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" defer></script>
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.css">
        <div class="container-fluid">

            <!-- Page Heading -->
                <a href="<?php echo base_url().'dashboard' ?>">Back</a>
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">List Pegawai</h1>
            </div>

               
            <!-- Content Row -->

            <div class="row">
                <!-- Area Chart -->
                <div class="col-xl-12 col-lg-12">
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>NIP Pegawai</th>
                            <th>Nama Pegawai</th>
                            <th>Email Pegawai</th>
                            <th width=250px style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                        $i=1;
                        foreach($project as $row)
                        { ?>
                        <tr>
                            <td></td>
                            <td><?= $row['NIP']?></td>
                            <td><?= $row['firstname']?></td>
                            <td><?= $row['email']?></td>
                            <td style="text-align: center;">
                                <button type="button" class="btn btn-warning"  onclick="location.href='<?php echo base_url();?>ProjectPegawai/index/<?= $row['id']?>'"><i class="fas fa-eye"></i> View Project</button>
                            </td>
                        </tr>
            <?php } ?>
        </tbody>
    </table>
                    
                </div>  
            </div>


        </div>
        <!-- /.container-fluid -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
<script type="text/javascript">
   $(document).ready(function() {
    var t = $('#example').DataTable({
        "scrollX" : true,
        "order": [[ 2, "desc" ]],
        "columnDefs": [ {
            "targets": 6,
            "orderable": false
        } ],
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ]
    });
    
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $('#editProjectModal').on('show.bs.modal', function(e) {
        var projectId = $(e.relatedTarget).data('id');
        var projectName = $(e.relatedTarget).data('name');
        $(this).find("input[type=text]").val(projectName);
        $(this).find("#projectId").val(projectId);        
    })

} );
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>
      