        <!-- Begin Page Content -->
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" defer></script>

        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.4/css/jquery.dataTables.css">
        <div class="container-fluid">

            <!-- Page Heading -->
                <a href="<?php echo base_url().'pegawai' ?>">Back</a>
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">List Project</h1>
                <a href="<?php echo site_url('ProjectPegawai/addProjectshow/'.$id) ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Tambah Project </a>
            </div>
                
            <!-- Content Row -->

            <div class="row">
                <!-- Area Chart -->
                <div class="col-xs-12 col-md-12">
                
                  <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nama Project</th>
                            <th>Tanggal Project</th>
                            <th>Status Project</th>
                            <th>Update Terakhir</th>
                            <th>Approval</th>
                            <th width=250px style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    <?php
                        $i=1;
                        foreach($project as $row)
                        { ?>
                        <tr>
                        <form action="<?php echo base_url().'ProjectPegawai/aproval'; ?>" method="POST">
                        <input type="hidden" class="form-control bg-light border-0 small" name="idProject" value="<?= $row['idProject']?>" aria-label="Search" aria-describedby="basic-addon2">
                        <input type="hidden" class="form-control bg-light border-0 small" name="idPegawai"value="<?= $row['idPegawai']?>" aria-label="Search" aria-describedby="basic-addon2">
                            <td></td>
                            <td><?= $row['namaProject']?></td>
                            <td><?= $row['tanggalProject']?></td>
                            <td><?= $row['statusProject']?></td>
                            <td><?= $row['Nama_Jabatan']?></td>
                            <td style="text-align: center;">
                            <?php if($row['approval']== 0 ) {?>
                            <button type="submit" class="btn btn-success" data-toggle="modal" data-target="#approvalModal" style="width:111px"><i class="fas fa-check"></i> Approve</button>
                            <?php }else {?>
                                <i class="fas fa-check-square"></i>
                            <?php }?>
                            </td>
                            <td style="text-align:center;min-width:270px">
                            <?php if($row['statusProject']== "Finished" ) { ?><button type="button" 
                                    <?php if($row['pathDoc']== '' ) {?>
                                    disabled
                                    <?php } ?>
                                    class="btn btn-primary" style="width: 125px; margin: 1px;" onclick="location.href='<?php echo base_url();?>listproject/download/<?= $row['pathDoc']?>'" ><i class="fas fa-file-download"></i> Download</button>
                            <?php } ?>
                            <button type="button" class="btn btn-info" style="width: 125px; margin: 1px;" onclick="location.href='<?php echo base_url();?>taskPegawai/viewTask/<?= $row['idPegawai']?>/<?= $row['idProject']?>'"><i class="fas fa-eye"></i> View Task</button>
                            <?php if($row['statusProject']== "Tahap Awal" ) {?>
                            <button type="button" class="btn btn-danger" style="width: 125px; margin: 1px;" onclick="deleteConfirm('<?php echo site_url('taskPegawai/delete/'.$row['idProject']) ?>')"><i class="fas fa-trash"></i> Delete</button>
                            <?php } ?>
                            </td>
                            </form>
                        </tr>
            <?php } ?>
            
        </tbody>
    </table>
    

                    
                </div>  
            </div>


        </div>
        <!-- /.container-fluid -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>
<script type="text/javascript">
   $(document).ready(function() {
    var t = $('#example').DataTable({
        "scrollX" : true,
        "order": [[ 2, "desc" ]],
        "columnDefs": [ {
            "targets": 6,
            "orderable": false
        } ],
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ]
    });
    
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $('#editProjectModal').on('show.bs.modal', function(e) {
        var projectId = $(e.relatedTarget).data('id');
        var projectName = $(e.relatedTarget).data('name');
        $(this).find("input[type=text]").val(projectName);
        $(this).find("#projectId").val(projectId);        
    })

} );
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>
      